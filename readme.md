# CIGeoE Copy Paste Feature 3D

Copy and paste features from one layer to another of the same type preserving original Z coordinate

# Installation

For QGIS 2:



1- Copy the folder "CIGeoECopyPasteFeatures3D" to folder:

```console
[drive]:\Users\[user]\.qgis2\python\plugins
```

2 - Start QGis and go to menu "Plugins" and select "Manage and Install Plugins"

![ALT](./images/image01.png)

3 - Select “CIGeoE Copy Paste Features 3D”

![ALT](./images/image02.png)

4 - After confirm the operation the plugin will be available in toolbar

![ALT](./images/image03.png)

- Method 2:

1 - Compress the folder “cigeoe_toggle_vertex_visibility” (.zip).

2 - Start QGis and go to "Plugins" and select “Manage and Install Plugins”

3 - Select “Install from Zip” (choose .zip generated in point 1) and then select "Install Plugin"



# Usage

1 - Select a layer from Layers Panel. 

![ALT](./images/image04.png)

2 - Click on the plugin icon. Using your mouse while pressing the left button,  draw a rectangle above the features you want to select.

![ALT](./images/image05.png)

3 - After releasing the mouse button, a combo box appears on the screen. You must select the layer which will receive the copied features (the features appear on the screen in yellow).

![ALT](./images/image06.png)

4 - Click OK to do the copy. The destination layer will appear in edition mode.


# Contributing

Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

# License

[AGPL 3.0](https://www.gnu.org/licenses/agpl-3.0.en.html)
