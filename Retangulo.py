# -*- coding: utf-8 -*-
#from qgis.gui import QgsMapTool, QgsRubberBand, QgsMapToolEmitPoint 
from qgis.gui import *
#from qgis.core import QGis, QgsPoint, QgsRectangle, QgsMapLayer, QgsFeature, QgsDataSourceURI, QgsCoordinateReferenceSystem, QgsCoordinateTransform, QgsGeometry 
from qgis.core import *
from PyQt4 import QtCore, QtGui 
from PyQt4.QtGui import QColor, QMenu, QCursor, QMessageBox 
from qgis.utils import iface 

from PyQt4.QtCore import Qt 
 

class Retangulo(QgsMapToolEmitPoint): 
           
    def __init__(self, canvas, iface, dlg):
        """
        draws a rectangle, then features that intersect this rectangle are added to selection 
        """
        # Store reference to the map canvas
        self.canvas = canvas  

        self.iface = iface        # add iface for work with active layer    

        self.dlg = dlg         
                
        QgsMapToolEmitPoint.__init__(self, self.canvas)
        
        self.rubberBand = QgsRubberBand(self.canvas, geometryType=QGis.Line)
        self.rubberBand.setColor(QColor(0, 0, 240, 100))
        self.rubberBand.setWidth(1)
        
        self.reset() 
               

    def reset(self):
        self.startPoint = self.endPoint = None
        self.isEmittingPoint = False
        self.rubberBand.reset( QGis.Polygon )


    def canvasPressEvent(self, e):
        """
        Method used to build rectangle if shift is held, otherwise, feature select/deselect and identify is done.
        """
        self.startPoint = self.toMapCoordinates(e.pos())
        self.endPoint = self.startPoint
        self.isEmittingPoint = True
        
        self.showRect(self.startPoint, self.endPoint)
        
    #self.isEmittingPoint is used to build the rubberband on the method on CanvasMoveEvent

            
    def canvasMoveEvent(self, e):
        """
        Used only on rectangle select.
        """
        if not self.isEmittingPoint:
            return
        self.endPoint = self.toMapCoordinates( e.pos() )
        self.showRect(self.startPoint, self.endPoint)

   
    def showRect(self, startPoint, endPoint):
        """
        Builds rubberband rect.
        """
        self.rubberBand.reset(QGis.Polygon)
        if startPoint.x() == endPoint.x() or startPoint.y() == endPoint.y():
            return
        point1 = QgsPoint(startPoint.x(), startPoint.y())
        point2 = QgsPoint(startPoint.x(), endPoint.y())
        point3 = QgsPoint(endPoint.x(), endPoint.y())
        point4 = QgsPoint(endPoint.x(), startPoint.y())

        self.rubberBand.addPoint(point1, False)
        self.rubberBand.addPoint(point2, False)
        self.rubberBand.addPoint(point3, False)
        self.rubberBand.addPoint(point4, True)    # true to update canvas
        self.rubberBand.show()


    def rectangle(self):
        """
        Builds rectangle from self.startPoint and self.endPoint
        """
        if self.startPoint is None or self.endPoint is None:
            return None
        elif self.startPoint.x() == self.endPoint.x() or self.startPoint.y() == self.endPoint.y():
            return None
        return QgsRectangle(self.startPoint, self.endPoint)

    
    def canvasReleaseEvent(self, e):
        """
        After the rectangle is built, here features are selected.
        """
        self.isEmittingPoint = False

        r = self.rectangle()            #QgsRectangle
        
        if r is not None:
            
            layerActive = self.iface.activeLayer()   # work with active layer  
            #if not layerActive.isEditable():
                #QMessageBox.information(self.iface.mainWindow(), "Error", 'Layer is not editable.')
                #self.rubberBand.hide()
                #return               
            
            rectSelFeats=[]              
                        
            bbRect = self.canvas.mapSettings().mapToLayerCoordinates(layerActive, r) 
            for f in layerActive.getFeatures(QgsFeatureRequest(bbRect)):                 
                rectSelFeats.append( f ) 
                        
            self.rubberBand.hide()
           
            if(len(rectSelFeats) == 0): 
                QMessageBox.information(self.iface.mainWindow(), "Error", 'No features selected.')
                self.rubberBand.hide()
                return             
           
            # select features 
            box = layerActive.getFeatures(QgsFeatureRequest(bbRect))
            ifeat = [i.id() for i in box]
            layerActive.setSelectedFeatures( ifeat ) 


            self.dlg.show()
           
            layers = self.iface.legendInterface().layers()    # list with all layers visible or not
            #layers = self.canvas.layers()                    # list with all layers visible
            
            layer_list = []
            comboList = []
            ind = 0
            for layer in layers: 
                if layer.type() != QgsMapLayer.RasterLayer:     
                    #  only layers of the same type         
                    if layer.geometryType() == layerActive.geometryType():
                        if layer.name() != layerActive.name():                 
                            layer_list.append(layer.name())
                            comboList.append(ind)
                    ind += 1

            if(len(layer_list) == 0): 
                QMessageBox.information(self.iface.mainWindow(), "Error", 'No layer selected.')
                self.rubberBand.hide()
                layerActive.setSelectedFeatures([])    # unselect all features selected
                return 

            self.dlg.comboBox.addItems(layer_list)
            
            result = self.dlg.exec_()
           
            if result == 1:
                selectedLayerIndex = self.dlg.comboBox.currentIndex()
                #selectedLayer = layers[selectedlayerIndex]                        
            else:
                QMessageBox.information(self.iface.mainWindow(), "Error", 'Canceled layer selection.' )   
                self.dlg.comboBox.clear() 
                layerActive.setSelectedFeatures([])    # unselect all features selected   
                return 

            indlayer = 0
            for x1 in range(len(layer_list)):
                if x1 == selectedLayerIndex:
                    indlayer = comboList[x1]                        
                    selectedLayer = layers[indlayer]  

            self.dlg.comboBox.clear() 

            layerActive.setSelectedFeatures([])    # unselect all features selected
                     
            data_provider = selectedLayer.dataProvider()
            
            caps = data_provider.capabilities()
            # Check if a particular capability is supported:
            caps1 = caps & QgsVectorDataProvider.AddFeatures    # 1 if AddFeatures is supported
            if caps1 != 1:     
                QMessageBox.information(self.iface.mainWindow(), "Error", 'The selected layer is not allowed to add features' )                  
                return   

            selectedLayer.startEditing()
                 
            for feat in rectSelFeats:

                newfeat=QgsFeature()  
                
                #newfeat.setGeometry(feat.geometry())
                #newfeat.setFields(feat.fields())  
                newfeat.setFields(selectedLayer.fields())     # fields of selectedLayer
                      
                for field in feat.fields():   
                    for field1 in selectedLayer.fields():
                        # select fields with the same name
                        if field.name() == field1.name():                 
                            newfeat.setAttribute(field.name(), feat[field.name()])      

                newfeat.setGeometry(feat.geometry())               
                
                data_provider.addFeatures([newfeat])                 
                
                                                                     
            selectedLayer.triggerRepaint()                           
           
            self.iface.messageBar().pushMessage("CIGeoE Copy Paste Features 3D", "Copy and Paste done" , level=QgsMessageBar.INFO, duration=3)
                
            
            

