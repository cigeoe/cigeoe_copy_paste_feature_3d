# -*- coding: utf-8 -*-
"""
/***************************************************************************
 CIGeoECopyPasteFeatures3D
                                 A QGIS plugin
 Copy and paste features from one layer to another of the same type
                             -------------------
        begin                : 2019-06-06
        copyright            : (C) 2019 by Centro de Informação Geoespacial do Exército
        email                : igeoe@igeoe.pt
        git sha              : $Format:%H$
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
 This script initializes the plugin, making it known to QGIS.
"""


# noinspection PyPep8Naming
def classFactory(iface):  # pylint: disable=invalid-name
    """Load CIGeoECopyPasteFeatures3D class from file CIGeoECopyPasteFeatures3D.

    :param iface: A QGIS interface instance.
    :type iface: QgsInterface
    """
    #
    from .CIGeoE_Copy_Paste_Feature_3D import CIGeoECopyPasteFeatures3D
    return CIGeoECopyPasteFeatures3D(iface)
